
import React, {useContext, useEffect, useState} from 'react';
import {Button,  StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {GameContext, TokenIdContext} from "./Contexts";
import RpsClientApi from "./RpsClientApi";


const PlayerGame = ({navigation}) => {
    const [sign, setSign] = useState('');
    const tokenId = useContext(TokenIdContext);
    const [game,setGame] = useState(GameContext);

    useEffect(() => {
        setTimeout(() => {
            RpsClientApi.getGameStatus(tokenId, setGame).catch(err=>console.log(err));
        }, 10000)
    }, [tokenId, game, setGame]);

    const makeMove=(sign, tokenId)=>{
        RpsClientApi.makeMove(tokenId, sign).then(res => setSign(res)).catch(err=>console.error(err));
    };
    return(
        <View style={styles.container}>

            <Text>id = {RpsClientApi.gameId()} </Text>
            <Text>Owner Information </Text>
            <Text>ownerToken= {RpsClientApi.ownerTokenId()} , ownerName={RpsClientApi.ownerName()}</Text>
            <TouchableOpacity style={styles.button} >
                <Button style={styles.leftBtn}
                        title="Paper"
                        onPress={() => makeMove("PAPER", tokenId)}
                />
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} >
                <Button
                    title="Rock"
                    onPress={() => makeMove("ROCK",tokenId)}
                />
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} >
                <Button
                    title="Scissors"
                    onPress={() => makeMove("SCISSORS", tokenId)}
                />
            </TouchableOpacity>
            <Text>Opponent Information</Text>

            <Text>Joiner Token= {RpsClientApi.opponentTokenId()} , JoinerName={RpsClientApi.opponentName()}</Text>
            <TouchableOpacity style={styles.button} >
                <Button style={styles.leftBtn}
                        title="Paper"
                        onPress={() => makeMove("PAPER", game.opponentTokenId)}
                />
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} >
                <Button
                    title="Rock"
                    onPress={() => makeMove("ROCK",game.opponentTokenId)}
                />
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} >
                <Button
                    title="Scissors"
                    onPress={() => makeMove("SCISSORS", game.opponentTokenId)}
                />
            </TouchableOpacity>

            <TouchableOpacity style={styles.button} >
                <Button
                    title="To see Result"
                    onPress={() => navigation.navigate("ShowResult")}
                />
            </TouchableOpacity>
        </View>
    );
}

const styles=StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#abc',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    leftBtn: {

    }


});
export default PlayerGame;